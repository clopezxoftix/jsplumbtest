import { Component, AfterContentInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { jsPlumb, jsPlumbInstance } from 'node_modules/jsplumb';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('lista1', { static: false }) lista1: ElementRef; 
  @ViewChild('lista2', { static: false }) lista2: ElementRef;

  public jsPlumbInstance;
  public atributos1: any;
  public atributos2: any; 
  public ul1: any;
  public ul2: any;
  public listLi: any;
  public listEDitRelacion: any = []; 
  public dsd: any;
  public tabla1 = {
    nombre: 'ac_caso_tb',
    atributos: ['id', 'numero', 'fechahoracreacion', 'refexp', 'id_fase']
  }

  public tabla2 = {
    nombre: 'ds_fasecaso_v',
    atributos: ['id', 'id_descriptor', 'codigo', 'nombre', 'activo']
  }

  listTabsCanvas: any = [ {tab: Object.assign({},this.tabla1)},{tab: Object.assign({},this.tabla2)} ];

  listAtrEdit: any = [];

  ngAfterViewInit() {

    this.jsPlumbInstance = jsPlumb.getInstance({
      Endpoint: ["Dot", {radius: 2}],
        Connector:"StateMachine",
        HoverPaintStyle: {stroke: "#1e8151", strokeWidth: 2 },
        ConnectionOverlays: [
            [ "Arrow", {
                location: 1,
                id: "arrow",
                length: 14,
                foldback: 0.8
            } ]
        ],
        Container: "canvas"
    });

    this.atributos1 = this.lista1.nativeElement;
    this.atributos2 = this.lista2.nativeElement; 
    this.ul1 = this.atributos1.querySelector("ul");
    this.ul2 = this.atributos2.querySelector("ul");

    this.jsPlumbInstance.draggable(this.atributos1);
    this.jsPlumbInstance.draggable(this.atributos2);
    
    this.listLi = this.jsPlumbInstance.getSelector('ul');
    
    this.jsPlumbInstance.batch(function () {});

    this.listLi.forEach(element1 => {
      let items = element1.querySelectorAll("li");
      items.forEach(element2 => {
        this.jsPlumbInstance.makeSource(element2, {
          allowLoopback: false,
          anchor: ["Left", "Right" ]
        });
        this.jsPlumbInstance.makeTarget(element2, {
          allowLoopback: false,
          anchor: ["Left", "Right" ]
        });
      });
    });
    
    this.jsPlumbInstance.addList(this.ul1, {
      endpoint:["Rectangle", {width:5, height:5}]
    });

    this.jsPlumbInstance.addList(this.ul2, {
      endpoint:["Rectangle", {width:5, height:5}]
    });

    this.jsPlumbInstance.bind("click", function(c) { this.jsPlumbInstance.deleteConnection(c); });
 
    this.jsPlumbInstance.bind("connection", function(info, originalEvent) {
      let vars = document.getElementById(info.targetId).textContent;
    });
    
    let listRelation = [];

    this.jsPlumbInstance.bind("connection", function(info) {
      listRelation.push({source: info.source.innerHTML, target:info.target.innerHTML });
      console.log(listRelation);
    });
    
  }

  checkEmiter(event, tab, atr){

    let obj = { tab, atr } ; 
    if(event.target.checked){
      this.listAtrEdit.push(obj);
    }
    if(!event.target.checked){
      this.listAtrEdit.splice(this.listAtrEdit.indexOf(obj) , 1 );
    }
  }

  cambAtr(atrNew, obj ){
    
    this.listTabsCanvas.forEach((element, indexTab) => {
      if(element.tab.nombre === obj.tab.nombre){
        element.tab.atributos.forEach( (atrTab, indexAtr) => {
          if(atrTab === obj.atr){
            this.listTabsCanvas[indexTab].tab.atributos[indexAtr] = atrNew;
          }
        });
      }
    });
    this.listAtrEdit.splice(this.listAtrEdit.indexOf(obj) , 1 );
    
  }

  updateEditRelation(){
    this.listEDitRelacion = [];
    let list = this.jsPlumbInstance.getConnections();
    
    list.forEach(element => {
      this.listEDitRelacion.push({source: element.source.innerHTML, target: element.target.innerHTML})
    });
    console.log(this.listEDitRelacion);
  }

  showJSON(){
    let json = [];
    let container = this.jsPlumbInstance.getContainer().childNodes;

    container.forEach(element => {
      if (element.querySelector('p')) {
        json.push({
          name: element.querySelector('p').innerHTML,
          top: element.getBoundingClientRect().top,
          rigth: element.getBoundingClientRect().right,
          bottom: element.getBoundingClientRect().bottom,
          left: element.getBoundingClientRect().left
        });     
      }
    });
    console.log(json);
  }
  /*   dragoverTabla(e){
    e.preventDefault();
    console.log('estoy siendo arrastrado');
  }

  dropTabla(){
    const id = "tabla#"+this.i ;
    this.listaTablas.push({ id : id, titulo: 'tabla'+ this.i });
    this.i+=1;
  }

  dragendTabla(){
    const d = this.i-1;
    this.jsPlumbInstance.draggable("tabla#"+ d);
    this.jsPlumbInstance.addEndpoint('tabla#'+ d, { anchor: 'Right'}, 
    this.exampleGreyEndpointOptions);
  } */


}
